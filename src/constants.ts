export const GITLAB_COM_URL = 'https://gitlab.com';
export const REVIEW_URI_SCHEME = 'gl-review';
export const CONFIG_NAMESPACE = 'gitlab';
export const CONFIG_CUSTOM_QUERIES = 'customQueries';
export const ADDED = 'added';
export const DELETED = 'deleted';
export const RENAMED = 'renamed';
export const MODIFIED = 'modified';
